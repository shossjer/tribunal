
#ifndef GAMEPLAY_LEVEL_HPP
#define GAMEPLAY_LEVEL_HPP

#include <string>

namespace gameplay
{
	namespace level
	{
		void create(const std::string & filename);
		void destroy();
	}
}

#endif /* GAMEPLAY_LEVEL_HPP */
